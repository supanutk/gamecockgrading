﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GamecockGrading.Middlewares
{
    public class SessionData
    {
        public string UserId { get; set; } = default;
        public string DisplayName { get; set; } = default;
        public string PhotoUrl { get; set; } = default;
        public string Role { get; set; } = default;

        public static SessionData GetSession(ISession session)
        {
            var res = new SessionData
            {
                UserId = session.GetString("UserId"),
                DisplayName = session.GetString("DisplayName"),
                PhotoUrl = session.GetString("PhotoUrl"),
                Role = session.GetString("Role")
            };
            return res;
        }
    }
}
