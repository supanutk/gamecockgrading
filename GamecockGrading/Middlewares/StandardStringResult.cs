﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GamecockGrading.Middlewares
{
    public class StandardStringResult
    {
        public static string SaveError()
        {
            return "เกิดข้อผิดพลาด กรุณาตืดต่อผู้ดูแลระบบ!!";
        }
        public static string SaveSuccess()
        {
            return "บันทึกสำเร็จ";
        }
        public static string Error(string exceptionString)
        {
            return "Error : " + exceptionString;
        }
    }
}
