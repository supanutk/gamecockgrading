﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GamecockGrading.Middlewares
{
    public partial class Field
    {
        public class CheckAccessToken
        {
            public string AuthType { get; set; }
            public string AccessToken { get; set; }
        }

        public class UserIdentity
        {
            public bool IsAuthenticated { get; set; }
            public string UserId { get; set; }
            public string Email { get; set; }
            public string DisplayName { get; set; }
            public string AuthType { get; set; }
            public string AccessToken { get; set; }
            public string PhotoUrl { get; set; }
            public string Role { get; set; }
        }
    }
}
