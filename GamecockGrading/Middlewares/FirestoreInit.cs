﻿using Google.Cloud.Firestore;
using Google.Cloud.Storage.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace GamecockGrading.Middlewares
{
    public class FirestoreInit
    {
        private static string projectId = "gamecockgrading";
        private static string bucketName = "gamecockgrading.appspot.com";
        private static string contentType = "auto";
        private static string credFilePath = AppDomain.CurrentDomain.BaseDirectory + "gamecockgrading-fe1dbe7fb4.json";
        public FirestoreDb Db { get; private set; }

        public FirestoreInit()
        {
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", credFilePath);
            Db = FirestoreDb.Create(projectId);
        }

        public FirestoreDb Create()
        {
            return Db;
        }

        public static string GenSinger(string objectName)
        {
            UrlSigner urlSigner = UrlSigner.FromServiceAccountPath(Environment.GetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS")).WithSigningVersion(SigningVersion.V4);
            string url = urlSigner.Sign(bucketName, objectName, TimeSpan.FromHours(1), HttpMethod.Get);

            return url;
        }
    }
}
