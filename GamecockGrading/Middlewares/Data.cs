﻿using Firebase.Auth;
using Firebase.Database;
using Google.Cloud.Firestore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static GamecockGrading.Middlewares.Field;
using static GamecockGrading.Middlewares.User;

namespace GamecockGrading.Middlewares
{
    public partial class Data
    {
        private FirebaseClient Firebase { get; set; }

        public static FirestoreDb db;

        public Data()
        {
            Initial();
        }

        public async Task<FirebaseAuthLink> LoginWithOAuth(string authType, string accessToken)
        {
            FirebaseAuthLink userToken = null;
            try
            {
                int auth = 0;
                switch (authType)
                {
                    case "google.com":
                        auth = (int)FirebaseAuthType.Google;
                        break;
                    case "facebook.com":
                        auth = (int)FirebaseAuthType.Facebook;
                        break;
                    case "password":
                        auth = (int)FirebaseAuthType.EmailAndPassword;
                        break;
                    default:
                        auth = -1;
                        break;
                }
                var authProvider = new FirebaseAuthProvider(new FirebaseConfig(Startup._firebaseApi));
                userToken = await authProvider.SignInWithOAuthAsync((FirebaseAuthType)auth, accessToken);
                //var check = await CheckAccount(userToken.User.LocalId, userToken.User.Email, userToken.User.DisplayName);
            }
            catch (Exception ex)
            {
                //Log;
                Console.WriteLine("Error : " + ex.Message);
            }
            return userToken;
        }

        public async Task<UserIdentity> NewGetIdentity(Field.CheckAccessToken param)
        {
            var auth = await new Data().LoginWithOAuth(param.AuthType, param.AccessToken);

            if (auth != null)
            {
                UserData user = new Data().GetUserByEmail(auth.User.Email);
                if (user != null)
                {
                    if (user.Status == "Inactive" || user.Status == "Pending")
                    {
                        UserIdentity identitys = new UserIdentity()
                        {
                            UserId = auth.User.Email
                        };
                        return await Task.FromResult<UserIdentity>(identitys);
                    }
                    UserIdentity identity = new UserIdentity()
                    {
                        IsAuthenticated = true,
                        UserId = auth.User.Email,
                        Email = auth.User.Email,
                        DisplayName = auth.User.DisplayName,
                        AuthType = param.AuthType,
                        AccessToken = param.AccessToken,
                        PhotoUrl = auth.User.PhotoUrl,
                        Role = user.Role
                    };
                    return await Task.FromResult(identity);
                }
                else
                {
                    UserData newUser = new UserData()
                    {
                        Email = auth.User.Email,
                        Name = auth.User.DisplayName,
                        Source = param.AuthType,
                        PhoneNo = auth.User.PhoneNumber,
                        Status = "Pending",
                        Role = "User"
                    };
                    new User().SavePending(newUser);
                }
            }
            UserIdentity identityss = new UserIdentity()
            {
                UserId = auth.User.Email
            };
            return await Task.FromResult(identityss);
        }

        public dynamic GetUserByEmail(string email)
        {
            var res = new User().CheckUser(email);
            if (res != null)
            {
                UserData result = JsonConvert.DeserializeObject<UserData>(res);
                return result;
            }
            return res;
        }

        private void Initial(FirebaseOptions option = null)
        {
            this.Firebase = new FirebaseClient(Startup._firebaseUrl, option);
        }
    }
}
