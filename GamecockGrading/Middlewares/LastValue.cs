﻿using Google.Cloud.Firestore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GamecockGrading.Middlewares
{
    public class LastValue
    {
        public static FirestoreDb db;
        public static readonly string CName = "LastValue";

        public LastValue()
        {
            db = new FirestoreInit().Create();
        }

        [FirestoreData]
        public class LastValueData
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public double FightGradeA { get; set; }
            [FirestoreProperty]
            public double FightGradeB { get; set; }
            [FirestoreProperty]
            public double FightGradeC { get; set; }
            [FirestoreProperty]
            public double FightGradeD { get; set; }
            [FirestoreProperty]
            public double RewardGradeA { get; set; }
            [FirestoreProperty]
            public double RewardGradeB { get; set; }
            [FirestoreProperty]
            public double RewardGradeC { get; set; }
            [FirestoreProperty]
            public double RewardGradeD { get; set; }
        }

        public class LastValUpdate
        {
            public string logKey { get; set; }
            public dynamic logValue { get; set; }
        }

        public string List(SessionData session)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(CName);
                DocumentSnapshot qrySnapshots = docRef.GetSnapshotAsync().Result;
                Dictionary<string, object> res = new Dictionary<string, object>();
                if (qrySnapshots.Exists)
                {
                    res = qrySnapshots.ToDictionary();
                }
                var jsC = JsonConvert.SerializeObject(res);
                Console.WriteLine(jsC);
                return jsC;
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public void Save(SessionData session, LastValUpdate data)
        {
            try
            {
                var js = List(session);
                DocumentReference docRef = db.Collection(CName).Document(CName);
                Dictionary<string, object> updates = new Dictionary<string, object>
                {
                    {data.logKey,data.logValue }
                };
                var updateRes = docRef.SetAsync(updates, SetOptions.MergeAll).Result;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
