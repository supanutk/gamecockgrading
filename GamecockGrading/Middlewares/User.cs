﻿using Google.Cloud.Firestore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GamecockGrading.Middlewares
{
    public class User
    {
        public static FirestoreDb db;
        public readonly string CName = "RegisteredUsers";

        public User()
        {
            db = new FirestoreInit().Create();
        }

        [FirestoreData]
        public class UserData
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string Email { get; set; }
            [FirestoreProperty]
            public string Name { get; set; }
            [FirestoreProperty]
            public string PhoneNo { get; set; }
            [FirestoreProperty]
            public string Source { get; set; } = "Web";
            [FirestoreProperty]
            public string Status { get; set; } = "Active";
            [FirestoreProperty]
            public string Role { get; set; }
            [FirestoreProperty]
            public string Address { get; set; }
            [FirestoreProperty]
            public string Username { get; set; }
            [FirestoreProperty]
            public string Password { get; set; }
        }

        [FirestoreData]
        public class UserDataUpdate
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string Name { get; set; }
            [FirestoreProperty]
            public string PhoneNo { get; set; }
            [FirestoreProperty]
            public string Address { get; set; }
            [FirestoreProperty]
            public string Role { get; set; }
            [FirestoreProperty]
            public string Username { get; set; }
            [FirestoreProperty]
            public string Password { get; set; }
        }

        public string ListAllUser()
        {
            List<UserData> listData = new List<UserData>();
            Query qry = db.Collection(CName);
            QuerySnapshot qrySnapshot = qry.GetSnapshotAsync().Result;
            foreach (DocumentSnapshot dss in qrySnapshot)
            {
                if (dss.Exists)
                {
                    var data = dss.ConvertTo<UserData>();
                    listData.Add(data);
                }
                else
                {
                    return StandardStringResult.SaveError();
                }
            }
            var jsP = JsonConvert.SerializeObject(listData);
            return jsP;
        }

        public string GetUserById(string docId)
        {
            DocumentReference docRef = db.Collection(CName).Document(docId);
            DocumentSnapshot snapshot = docRef.GetSnapshotAsync().Result;
            if (snapshot.Exists)
            {
                var res = snapshot.ConvertTo<UserData>();
                var jsp = JsonConvert.SerializeObject(res);
                return jsp;
            }
            else
            {
                return StandardStringResult.SaveError();
            }
        }

        public string CheckUser(string email)
        {
            DocumentReference docRef = db.Collection(CName).Document(email);
            DocumentSnapshot docRefSnapshot = docRef.GetSnapshotAsync().Result;
            if (docRefSnapshot.Exists)
            {
                var res = docRefSnapshot.ConvertTo<UserData>();
                var jsp = JsonConvert.SerializeObject(res);
                return jsp;
            }
            else
            {
                return null;
            }
        }

        public string CheckUsername(string username)
        {
            Query qry = db.Collection(CName).WhereEqualTo("Username", username);
            QuerySnapshot qrySnapshot = qry.GetSnapshotAsync().Result;
            if (qrySnapshot.Count >= 1)
            {
                return "ข้อมูลซ้ำ !";
            }
            else
            {
                return null;
            }
        }

        public void SavePending(UserData newUser)
        {
            DocumentReference docRef = db.Collection(CName).Document(newUser.Email);
            var res = docRef.SetAsync(newUser, SetOptions.MergeAll).Result.UpdateTime.ToString();
            Console.WriteLine(res);
        }

        public string ListPendingUser()
        {
            List<UserData> listData = new List<UserData>();
            Query qry = db.Collection(CName).WhereEqualTo("Status", "Pending");
            QuerySnapshot qrySnapshot = qry.GetSnapshotAsync().Result;
            foreach (DocumentSnapshot dss in qrySnapshot)
            {
                if (dss.Exists)
                {
                    var data = dss.ConvertTo<UserData>();
                    listData.Add(data);
                }
                else
                {
                    return StandardStringResult.SaveError();
                }
            }
            var jsP = JsonConvert.SerializeObject(listData);
            return jsP;
        }

        public string ChangeStatus(string docId, string status)
        {
            DocumentReference docRef = db.Collection(CName).Document(docId);
            Dictionary<string, object> update = new Dictionary<string, object>()
            {
                { "Status",status }
            };
            var res = docRef.UpdateAsync(update).Result.UpdateTime.ToString();
            if (res != "")
                return StandardStringResult.SaveSuccess();

            return StandardStringResult.SaveError();
        }

        public string SaveFromView(UserData formData)
        {
            DocumentReference docRef = db.Collection(CName).Document(formData.Email);
            var res = docRef.SetAsync(formData).Result.UpdateTime.ToString();
            if (res != "")
                return StandardStringResult.SaveSuccess();

            return StandardStringResult.SaveError();
        }

        public string ChangeRole(UserDataUpdate update)
        {
            DocumentReference docRef = db.Collection(CName).Document(update.DocId);
            var res = docRef.SetAsync(update, SetOptions.MergeAll).Result.UpdateTime.ToString();
            if (res != "")
                return StandardStringResult.SaveSuccess();

            return StandardStringResult.SaveError();
        }

        public string SaveRegister(UserData formData)
        {
            DocumentReference docRef = db.Collection(CName).Document(formData.Email);
            var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();
            if (res == null)
                return StandardStringResult.SaveError();

            return "ลงทะเบียนสำเร็จ !";
        }

        public string AncientLogin(string username,string password)
        {
            Query qry = db.Collection(CName).WhereEqualTo("Username", username).WhereEqualTo("Password", password);
            QuerySnapshot qrySnapshot = qry.GetSnapshotAsync().Result;
            UserData res = new UserData();
            foreach (DocumentSnapshot dss in qrySnapshot)
            {
                if (dss.Exists)
                {
                    res = dss.ConvertTo<UserData>();
                }
                else
                {
                    return StandardStringResult.SaveError();
                }
            }
            if (qrySnapshot.Count == 0)
            {
                return null;
            }
            else
            {
                var jsp = JsonConvert.SerializeObject(res);
                return jsp;
            }
        }
    }
}
