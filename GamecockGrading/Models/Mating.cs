﻿using GamecockGrading.Middlewares;
using Google.Cloud.Firestore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static GamecockGrading.Models.ChickenData;

namespace GamecockGrading.Models
{
    public class Mating
    {
        public static FirestoreDb db;
        public static readonly string CName = "Mating";
        public static readonly string ChickCol = "ChickenDatas";

        public Mating()
        {
            db = new FirestoreInit().Create();
        }

        public class AllDataById
        {
            public MatingData Data { get; set; }
            public List<ChickData> Chicks { get; set; }
        }

        [FirestoreData]
        public class MatingData
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string[] Chicks { get; set; } = { };
            [FirestoreProperty]
            public string CockId { get; set; }
            [FirestoreProperty]
            public string CockName { get; set; }
            [FirestoreProperty]
            public string CreateBy { get; set; }
            [FirestoreProperty]
            public string CreateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
            [FirestoreProperty]
            public string HenId { get; set; }
            [FirestoreProperty]
            public string HenName { get; set; }
            [FirestoreProperty]
            public string MatingDate { get; set; }
            [FirestoreProperty]
            public double DeleteStatus { get; set; } = 0;
            [FirestoreProperty]
            public string UpdateBy { get; set; }
            [FirestoreProperty]
            public string UpdateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
        }

        [FirestoreData]
        public class MatingDataUpdate
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string CockId { get; set; }
            [FirestoreProperty]
            public string CockName { get; set; }
            [FirestoreProperty]
            public string HenId { get; set; }
            [FirestoreProperty]
            public string HenName { get; set; }
        }

        public string List()
        {
            try
            {
                Query qry = db.Collection(CName).WhereEqualTo("DeleteStatus", 0);
                QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                List<MatingData> listQry = new List<MatingData>();
                foreach (DocumentSnapshot dss in qrySnapshots)
                {
                    if (dss.Exists)
                    {
                        var data = dss.ConvertTo<MatingData>();
                        listQry.Add(data);
                    }
                    else
                    {
                        return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";
                    }
                }
                var jsP = JsonConvert.SerializeObject(listQry);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string GetById(string docId)
        {
            try
            {
                DocumentReference qry = db.Collection(CName).Document(docId);
                DocumentSnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                AllDataById allData = new AllDataById();
                if (qrySnapshots.Exists)
                {
                    var data = qrySnapshots.ConvertTo<MatingData>();
                    allData.Data = data;
                    List<ChickData> listChick = new List<ChickData>();
                    foreach (var item in data.Chicks)
                    {
                        DocumentReference qry2 = db.Collection(ChickCol).Document(item);
                        DocumentSnapshot qrySnapshots2 = qry2.GetSnapshotAsync().Result;
                        var chick = qrySnapshots2.ConvertTo<ChickData>();
                        listChick.Add(chick);
                    }
                    allData.Chicks = listChick;
                }
                var jsP = JsonConvert.SerializeObject(allData);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string Save(MatingData formData)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document();
                var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";

                return "บันทึกสำเร็จ !!";
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string Update(MatingDataUpdate formData)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(formData.DocId);
                var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";

                return "บันทึกสำเร็จ !!";
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string Delete(string docId)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(docId);
                Dictionary<string, object> updates = new Dictionary<string, object>
                {
                    { "DeleteStatus", 1 }
                };
                var res = docRef.SetAsync(updates, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";

                return "ยกเลิกข้อมูลเรียบร้อย !!";
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string SaveChild(ChickData formData, string docId)
        {
            try
            {
                DocumentReference docRef = db.Collection(ChickCol).Document();
                var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();

                DocumentReference qry = db.Collection(CName).Document(docId);
                DocumentSnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                if (qrySnapshots.Exists)
                {
                    var data = qrySnapshots.ConvertTo<MatingData>();
                    Dictionary<string, object> update = new Dictionary<string, object>();
                    List<string> newChicks = new List<string>();
                    foreach (var item in data.Chicks)
                    {
                        newChicks.Add(item);
                    }
                    newChicks.Add(docRef.Id);
                    update.Add("Chicks", newChicks);
                    var res2 = db.Collection(CName).Document(docId).SetAsync(update, SetOptions.MergeAll).Result.UpdateTime.ToString();

                }

                if (res == null)
                    return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";

                return docRef.Id;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }
    }
}
