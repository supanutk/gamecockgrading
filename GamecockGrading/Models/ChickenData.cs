﻿using GamecockGrading.Middlewares;
using Google.Cloud.Firestore;
using Google.Rpc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static GamecockGrading.Models.Fighting;

namespace GamecockGrading.Models
{
    public class ChickenData
    {
        public static FirestoreDb db;
        public static readonly string CName = "ChickenDatas";
        public static readonly string SName = "Vaccines";

        public ChickenData()
        {
            db = new FirestoreInit().Create();
        }

        [FirestoreData]
        public class ChickData
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string BirthDay { get; set; }
            [FirestoreProperty]
            public string Code { get; set; }
            [FirestoreProperty]
            public string CreateBy { get; set; }
            [FirestoreProperty]
            public string CreateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
            [FirestoreProperty]
            public string DadId { get; set; }
            [FirestoreProperty]
            public string DadName { get; set; }
            [FirestoreProperty]
            public string[] Fightings { get; set; } = { };
            [FirestoreProperty]
            public double Height { get; set; } = 0;
            [FirestoreProperty]
            public string MomId { get; set; }
            [FirestoreProperty]
            public string MomName { get; set; }
            [FirestoreProperty]
            public string Name { get; set; }
            [FirestoreProperty]
            public string Sex { get; set; }
            [FirestoreProperty]
            public string Status { get; set; } = "Alive";
            [FirestoreProperty]
            public string Status_Th { get; set; } = "มีชีวิต";
            [FirestoreProperty]
            public string UpdateBy { get; set; }
            [FirestoreProperty]
            public string UpdateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
            [FirestoreProperty]
            public string Upload { get; set; }
            [FirestoreProperty]
            public double Weight { get; set; } = 0;
            [FirestoreProperty]
            public double DeleteStatus { get; set; } = 0;
            [FirestoreProperty]
            public string[] Vaccinations { get; set; } = { };
            [FirestoreProperty]
            public double Win { get; set; } = 0;
            [FirestoreProperty]
            public double Lose { get; set; } = 0;
            [FirestoreProperty]
            public double Draw { get; set; } = 0;
            [FirestoreProperty]
            public double Reward { get; set; } = 0;
            [FirestoreProperty]
            public string FileName01 { get; set; }
            [FirestoreProperty]
            public string FileName02 { get; set; }
            [FirestoreProperty]
            public string FileName03 { get; set; }
            [FirestoreProperty]
            public string BuyDate { get; set; }
            [FirestoreProperty]
            public string BuyFrom { get; set; }
            [FirestoreProperty]
            public string Remark { get; set; }
            [FirestoreProperty]
            public string CreateType { get; set; }
            [FirestoreProperty]
            public string ColorPlate { get; set; }
            [FirestoreProperty]
            public string Species { get; set; }
            [FirestoreProperty]
            public string NextMating { get; set; } = DateTime.Now.ToString("yyyy-MM-dd");
            [FirestoreProperty]
            public string SoldDate { get; set; }
            [FirestoreProperty]
            public double SoldPrice { get; set; } = 0;
            [FirestoreProperty]
            public string SoldRemark { get; set; }
            [FirestoreProperty]
            public string RunningNo { get; set; }
        }

        [FirestoreData]
        public class SubVaccines
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string InjectionDate { get; set; }
            [FirestoreProperty]
            public string VcCode { get; set; }
            [FirestoreProperty]
            public string VcDetail { get; set; }
            [FirestoreProperty]
            public string VcName { get; set; }
        }

        [FirestoreData]
        public class ChickDataUpdate
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string BirthDay { get; set; }
            [FirestoreProperty]
            public string Code { get; set; }
            [FirestoreProperty]
            public string DadId { get; set; }
            [FirestoreProperty]
            public string DadName { get; set; }
            [FirestoreProperty]
            public double Height { get; set; } = 0;
            [FirestoreProperty]
            public string MomId { get; set; }
            [FirestoreProperty]
            public string MomName { get; set; }
            [FirestoreProperty]
            public string Name { get; set; }
            [FirestoreProperty]
            public string Sex { get; set; }
            [FirestoreProperty]
            public string UpdateBy { get; set; }
            [FirestoreProperty]
            public string UpdateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
            [FirestoreProperty]
            public string Upload { get; set; }
            [FirestoreProperty]
            public double Weight { get; set; } = 0;
            [FirestoreProperty]
            public string FileName01 { get; set; }
            [FirestoreProperty]
            public string FileName02 { get; set; }
            [FirestoreProperty]
            public string FileName03 { get; set; }
            [FirestoreProperty]
            public string BuyDate { get; set; }
            [FirestoreProperty]
            public string BuyFrom { get; set; }
            [FirestoreProperty]
            public string Remark { get; set; }
            [FirestoreProperty]
            public string CreateType { get; set; }
            [FirestoreProperty]
            public string ColorPlate { get; set; }
            [FirestoreProperty]
            public string Species { get; set; }
        }

        [FirestoreData]
        public class ChickDataToSold
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string SoldDate { get; set; }
            [FirestoreProperty]
            public double SoldPrice { get; set; }
            [FirestoreProperty]
            public string SoldRemark { get; set; }
            [FirestoreProperty]
            public string Status { get; set; } = "Sale";
            [FirestoreProperty]
            public string Status_Th { get; set; } = "ขาย";
        }

        public class FullDetailData
        {
            public ChickData Chick { get; set; }
            public List<Vaccination.AllDataById> Vaccinations { get; set; }
            public List<FightingData> Fighting { get; set; }
        }

        public class ChickDataForReport
        {
            public List<Dictionary<string, object>> Chick { get; set; }
            public Dictionary<string, object> Summary { get; set; }
        }

        public string List()
        {
            try
            {
                Query qry = db.Collection(CName).WhereEqualTo("DeleteStatus", 0).OrderBy("Code");
                QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                List<ChickData> listQry = new List<ChickData>();
                foreach (DocumentSnapshot dss in qrySnapshots)
                {
                    if (dss.Exists)
                    {
                        ChickData data = dss.ConvertTo<ChickData>();
                        listQry.Add(data);
                    }
                    else
                    {
                        return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";
                    }
                }
                var jsP = JsonConvert.SerializeObject(listQry);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string ListAlive()
        {
            try
            {
                Query qry = db.Collection(CName).WhereEqualTo("DeleteStatus", 0).WhereEqualTo("Status", "Alive").OrderBy("Code");
                QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                List<ChickData> listQry = new List<ChickData>();
                foreach (DocumentSnapshot dss in qrySnapshots)
                {
                    if (dss.Exists)
                    {
                        ChickData data = dss.ConvertTo<ChickData>();
                        listQry.Add(data);
                    }
                    else
                    {
                        return StandardStringResult.SaveError();
                    }
                }
                var jsP = JsonConvert.SerializeObject(listQry);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public string GetById(string docId)
        {
            try
            {
                DocumentReference qry = db.Collection(CName).Document(docId);
                DocumentSnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                ChickData res = new ChickData();
                if (qrySnapshots.Exists)
                {
                    res = qrySnapshots.ConvertTo<ChickData>();
                }
                var jsP = JsonConvert.SerializeObject(res);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string GetBySex(string sex)
        {
            try
            {
                Query qry = db.Collection(CName).WhereEqualTo("Sex", sex).WhereEqualTo("Status", "Alive").WhereEqualTo("DeleteStatus", 0);
                QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                List<ChickData> listQry = new List<ChickData>();
                foreach (DocumentSnapshot dss in qrySnapshots)
                {
                    if (dss.Exists)
                    {
                        ChickData data = dss.ConvertTo<ChickData>();
                        listQry.Add(data);
                    }
                    else
                    {
                        return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";
                    }
                }
                var jsP = JsonConvert.SerializeObject(listQry);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string Save(ChickData formData)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document();
                var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";

                return "บันทึกสำเร็จ !!";
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string Update(ChickDataUpdate formData)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(formData.DocId);
                var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";

                return "บันทึกสำเร็จ !!";
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string UpdateStatusToDead(string docId)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(docId);
                Dictionary<string, object> update = new Dictionary<string, object>()
                {
                    { "Status","Dead"},
                    { "Status_Th","ตาย"}
                };
                var res = docRef.UpdateAsync(update).Result.UpdateTime.ToString();

                if (res == null)
                    return StandardStringResult.SaveError();

                return StandardStringResult.SaveSuccess();
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public string UpdateStatusToSold(ChickDataToSold update)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(update.DocId);
                var res = docRef.SetAsync(update, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return StandardStringResult.SaveError();

                return StandardStringResult.SaveSuccess();
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public string Delete(string docId)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(docId);
                Dictionary<string, object> updates = new Dictionary<string, object>
                {
                    { "DeleteStatus", 1 }
                };
                var res = docRef.SetAsync(updates, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";

                return "ยกเลิกข้อมูลเรียบร้อย !!";
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string GetFullDetail(string docId)
        {
            try
            {
                Vaccination vacFunc = new Vaccination();
                Fighting fightingFunc = new Fighting();
                FullDetailData fullData = new FullDetailData();
                fullData.Chick = JsonConvert.DeserializeObject<ChickData>(GetById(docId));
                List<Vaccination.AllDataById> listVaccine = new List<Vaccination.AllDataById>();
                foreach (var vcId in fullData.Chick.Vaccinations)
                {
                    var vac = JsonConvert.DeserializeObject<Vaccination.AllDataById>(vacFunc.GetById(vcId));
                    listVaccine.Add(vac);
                }
                fullData.Vaccinations = listVaccine;
                List<FightingData> listFighting = new List<FightingData>();
                foreach (var fightId in fullData.Chick.Fightings)
                {
                    var fighting = JsonConvert.DeserializeObject<FightingData>(fightingFunc.GetById(fightId));
                    listFighting.Add(fighting);
                }
                fullData.Fighting = listFighting;

                var jsP = JsonConvert.SerializeObject(fullData);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string ListChickForReport()
        {
            try
            {
                Query qry = db.Collection(CName).WhereEqualTo("DeleteStatus", 0);
                QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                List<Dictionary<string, object>> listQry = new List<Dictionary<string, object>>();
                foreach (DocumentSnapshot dss in qrySnapshots)
                {
                    if (dss.Exists)
                    {
                        var data = dss.ToDictionary();
                        Query qry2 = db.Collection(CName);
                        var child = 0;
                        double sumReward = 0;
                        if (data["Sex"].ToString() == "ผู้")
                        {
                            qry2 = qry2.WhereEqualTo("DadId", dss.Id).WhereEqualTo("DeleteStatus", 0);
                        }
                        else
                        {
                            qry2 = qry2.WhereEqualTo("MomId", dss.Id).WhereEqualTo("DeleteStatus", 0);
                        }
                        QuerySnapshot snap2 = qry2.GetSnapshotAsync().Result;
                        foreach (DocumentSnapshot dss2 in snap2)
                        {
                            var data2 = dss2.ToDictionary();
                            sumReward += Double.Parse(data2["Reward"].ToString());
                        }
                        child = snap2.Count;
                        data["ChildCount"] = child;
                        data["SumReward"] = sumReward;
                        data["DocId"] = dss.Id;
                        listQry.Add(data);
                    }
                    else
                    {
                        return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";
                    }
                }
                var jsP = JsonConvert.SerializeObject(listQry);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string GetListChickById(string docId, string sex)
        {
            try
            {
                Query qry = db.Collection(CName).WhereEqualTo("DeleteStatus", 0);
                if (sex == "ผู้")
                {
                    qry = qry.WhereEqualTo("DadId", docId).OrderBy("Code");
                }
                else
                {
                    qry = qry.WhereEqualTo("MomId", docId).OrderBy("Code");
                }
                QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                ChickDataForReport allData = new ChickDataForReport();
                List<Dictionary<string, object>> listChick = new List<Dictionary<string, object>>();
                double sum = 0;
                double win = 0;
                double lose = 0;
                double draw = 0;
                foreach (DocumentSnapshot dss in qrySnapshots)
                {
                    if (dss.Exists)
                    {
                        DocumentReference docRef = db.Collection(CName).Document(dss.Id);
                        DocumentSnapshot docRefSnapshot = docRef.GetSnapshotAsync().Result;
                        if (docRefSnapshot.Exists)
                        {
                            var data = docRefSnapshot.ToDictionary();
                            sum += Double.Parse(data["Reward"].ToString());
                            win += Double.Parse(data["Win"].ToString());
                            lose += Double.Parse(data["Lose"].ToString());
                            draw += Double.Parse(data["Draw"].ToString());
                            var cWin = Double.Parse(data["Win"].ToString());
                            var cLose = Double.Parse(data["Lose"].ToString());
                            if (cWin == 0 && cLose == 0)
                            {
                                data["WinRate"] = "0 %";
                            }
                            else
                            {
                                data["WinRate"] = ((cWin / (cWin + cLose)) * 100) + " %";
                            }
                            listChick.Add(data);
                        }
                    }
                    else
                    {
                        return StandardStringResult.SaveError();
                    }
                }
                Dictionary<string, object> summary = new Dictionary<string, object>
                {
                    { "Sum",sum },
                    { "Win",win },
                    { "Lose",lose },
                    { "Draw",draw }
                };
                allData.Chick = listChick;
                allData.Summary = summary;
                var jsP = JsonConvert.SerializeObject(allData);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public bool CheckSameCode(string code)
        {
            try
            {
                Query qry = db.Collection(CName).WhereEqualTo("Code", code);
                QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                List<ChickData> listQry = new List<ChickData>();
                foreach (DocumentSnapshot dss in qrySnapshots)
                {
                    if (dss.Exists)
                    {
                        ChickData data = dss.ConvertTo<ChickData>();
                        listQry.Add(data);
                    }
                    else
                    {
                        return false;
                    }
                }
                if (listQry.Count != 0)
                    return false;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public string GetCockFullDetail()
        {
            try
            {
                Vaccination vacFunc = new Vaccination();
                Fighting fightingFunc = new Fighting();
                List<FullDetailData> listFullData = new List<FullDetailData>();
                List<ChickData> listChick = JsonConvert.DeserializeObject<List<ChickData>>(this.GetBySex("ผู้"));
                foreach (ChickData chickD in listChick)
                {
                    FullDetailData fullData = new FullDetailData();
                    fullData.Chick = JsonConvert.DeserializeObject<ChickData>(GetById(chickD.DocId));
                    List<Vaccination.AllDataById> listVaccine = new List<Vaccination.AllDataById>();

                    foreach (var vcId in fullData.Chick.Vaccinations)
                    {
                        var vac = JsonConvert.DeserializeObject<Vaccination.AllDataById>(vacFunc.GetById(vcId));
                        listVaccine.Add(vac);
                    }
                    fullData.Vaccinations = listVaccine;
                    List<FightingData> listFighting = new List<FightingData>();
                    foreach (var fightId in fullData.Chick.Fightings)
                    {
                        var fighting = JsonConvert.DeserializeObject<FightingData>(fightingFunc.GetById(fightId));
                        listFighting.Add(fighting);
                    }
                    fullData.Fighting = listFighting;
                    listFullData.Add(fullData);
                }

                var jsP = JsonConvert.SerializeObject(listFullData);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }
    }
}
