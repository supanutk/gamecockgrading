﻿using GamecockGrading.Middlewares;
using Google.Cloud.Firestore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static GamecockGrading.Models.ChickenData;
using static GamecockGrading.Models.VaccineData;

namespace GamecockGrading.Models
{
    public class Vaccination
    {
        public static FirestoreDb db;
        public static readonly string CName = "Vaccinations";
        public static readonly string VcCol = "Vaccines";
        public static readonly string ChickCol = "ChickenDatas";

        public Vaccination()
        {
            db = new FirestoreInit().Create();
        }

        public class AllDataById
        {
            public VaccinationData Data { get; set; }
            public List<ChickData> Chickens { get; set; }
        }

        [FirestoreData]
        public class VaccinationData
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string[] Chickens { get; set; }
            [FirestoreProperty]
            public string CreateBy { get; set; }
            [FirestoreProperty]
            public string CreateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
            [FirestoreProperty]
            public double DeleteStatus { get; set; } = 0;
            [FirestoreProperty]
            public string Detail { get; set; } = "";
            [FirestoreProperty]
            public string UpdateBy { get; set; }
            [FirestoreProperty]
            public string UpdateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
            [FirestoreProperty]
            public string VaccinationDate { get; set; }
            [FirestoreProperty]
            public string VcDocId { get; set; }
            [FirestoreProperty]
            public string VcName { get; set; }
        }

        [FirestoreData]
        public class VaccinationUpdate
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string[] Chickens { get; set; }
            [FirestoreProperty]
            public string Detail { get; set; } = "";
            [FirestoreProperty]
            public string UpdateBy { get; set; }
            [FirestoreProperty]
            public string UpdateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
        }

        [FirestoreData]
        public class ListChickAndLastVac
        {
            public VcData VacData { get; set; }
            public List<ChickAndLastVac> ChickLastVac { get; set; }
        }

        [FirestoreData]
        public class ChickAndLastVac
        {
            public ChickData ChickData { get; set; }
            public VaccinationData VacciData { get; set; }

        }

        public string List()
        {
            try
            {
                Query qry = db.Collection(CName).WhereEqualTo("DeleteStatus", 0);
                QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                List<VaccinationData> listQry = new List<VaccinationData>();
                foreach (DocumentSnapshot dss in qrySnapshots)
                {
                    if (dss.Exists)
                    {
                        VaccinationData data = dss.ConvertTo<VaccinationData>();
                        listQry.Add(data);
                    }
                    else
                    {
                        return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";
                    }
                }
                var jsP = JsonConvert.SerializeObject(listQry);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string GetById(string docId)
        {
            try
            {
                DocumentReference qry = db.Collection(CName).Document(docId);
                DocumentSnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                AllDataById allData = new AllDataById();
                if (qrySnapshots.Exists)
                {
                    var data = qrySnapshots.ConvertTo<VaccinationData>();
                    allData.Data = data;
                    List<ChickData> listChicken = new List<ChickData>();
                    foreach (var chickId in data.Chickens)
                    {
                        DocumentReference qry3 = db.Collection(ChickCol).Document(chickId);
                        DocumentSnapshot qrySnapshots3 = qry3.GetSnapshotAsync().Result;
                        var chicken = qrySnapshots3.ConvertTo<ChickData>();
                        listChicken.Add(chicken);
                    }
                    allData.Chickens = listChicken;
                }
                var jsP = JsonConvert.SerializeObject(allData);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string Save(VaccinationData formData)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document();
                var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return StandardStringResult.SaveError();

                return StandardStringResult.SaveSuccess();
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public string Update(VaccinationUpdate formData)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(formData.DocId);
                var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return StandardStringResult.SaveError();

                return StandardStringResult.SaveSuccess();
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public string Delete(string docId)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(docId);
                Dictionary<string, object> updates = new Dictionary<string, object>
                {
                    { "DeleteStatus", 1 }
                };
                var res = docRef.SetAsync(updates, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return StandardStringResult.SaveError();

                return "ยกเลิกข้อมูลเรียบร้อย !!";
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public string ListChickLastVac(string vcDocId)
        {
            try
            {
                ListChickAndLastVac listAllData = new ListChickAndLastVac();
                listAllData.VacData = JsonConvert.DeserializeObject<VcData>(new VaccineData().GetById(vcDocId));
                List<ChickData> listChick = JsonConvert.DeserializeObject<List<ChickData>>(new ChickenData().ListAlive());
                List<ChickAndLastVac> listChickAndLastVac = new List<ChickAndLastVac>();
                foreach (var chicks in listChick)
                {
                    ChickAndLastVac chickAndLastVac = new ChickAndLastVac();
                    chickAndLastVac.ChickData = chicks;

                    Query qry = db.Collection(CName).WhereEqualTo("VcDocId", vcDocId).WhereEqualTo("DeleteStatus", 0).WhereArrayContains("Chickens", chicks.DocId).OrderByDescending("VaccinationDate").Limit(1);
                    QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                    foreach (DocumentSnapshot dss in qrySnapshots)
                    {
                        chickAndLastVac.VacciData = dss.ConvertTo<VaccinationData>();
                    }
                    //if (qrySnapshots.Count == 0)
                    //{
                    //    chickAndLastVac.VacciData = new VaccinationData();
                    //}
                    listChickAndLastVac.Add(chickAndLastVac);
                }
                listAllData.ChickLastVac = listChickAndLastVac;

                var jsP = JsonConvert.SerializeObject(listAllData);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {

                return StandardStringResult.Error(e.Message);
            }
        }

        public string ListAllChickAllLastVac()
        {
            try
            {
                List<ListChickAndLastVac> listAllData = new List<ListChickAndLastVac>();
                List<VcData> listVaccine = JsonConvert.DeserializeObject<List<VcData>>(new VaccineData().List());
                foreach (var vac in listVaccine)
                {
                    ListChickAndLastVac allData = JsonConvert.DeserializeObject<ListChickAndLastVac>(ListChickLastVac(vac.DocId));
                    listAllData.Add(allData);
                }
                var jsP = JsonConvert.SerializeObject(listAllData);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }
    }
}
