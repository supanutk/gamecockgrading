﻿using GamecockGrading.Middlewares;
using Google.Cloud.Firestore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static GamecockGrading.Models.ChickenData;

namespace GamecockGrading.Models
{
    public class Fighting
    {
        public static FirestoreDb db;
        public static readonly string CName = "CockFightings";

        public Fighting()
        {
            db = new FirestoreInit().Create();
        }

        public class FightAllData
        {
            public FightingData fight { get; set; }
            public ChickData chick { get; set; }
        }

        [FirestoreData]
        public class FightingData
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public double Bet { get; set; } = 0;
            [FirestoreProperty]
            public string CockId { get; set; }
            [FirestoreProperty]
            public string CockName { get; set; }
            [FirestoreProperty]
            public string CreateBy { get; set; }
            [FirestoreProperty]
            public string CreateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
            [FirestoreProperty]
            public string FightingDate { get; set; }
            [FirestoreProperty]
            public string Location { get; set; }
            [FirestoreProperty]
            public string Result { get; set; }
            [FirestoreProperty]
            public double DeleteStatus { get; set; } = 0;
            [FirestoreProperty]
            public string UpdateBy { get; set; }
            [FirestoreProperty]
            public string UpdateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
            [FirestoreProperty]
            public string OpponentSpecies { get; set; }
            [FirestoreProperty]
            public string FightingRemark { get; set; }
        }

        [FirestoreData]
        public class FightingDataUpdate
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public double Bet { get; set; } = 0;
            [FirestoreProperty]
            public string CockId { get; set; }
            [FirestoreProperty]
            public string CockName { get; set; }
            [FirestoreProperty]
            public string FightingDate { get; set; }
            [FirestoreProperty]
            public string Location { get; set; }
            [FirestoreProperty]
            public string UpdateBy { get; set; }
            [FirestoreProperty]
            public string UpdateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
            [FirestoreProperty]
            public string OpponentSpecies { get; set; }
            [FirestoreProperty]
            public string FightingRemark { get; set; }
        }

        public string List()
        {
            try
            {
                Query qry = db.Collection(CName).WhereEqualTo("DeleteStatus", 0);
                QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                List<FightAllData> listAllData = new List<FightAllData>();
                foreach (DocumentSnapshot dss in qrySnapshots)
                {
                    if (dss.Exists)
                    {
                        FightAllData allData = new FightAllData();
                        allData.fight = dss.ConvertTo<FightingData>();

                        DocumentReference docRef = db.Collection("ChickenDatas").Document(allData.fight.CockId);
                        DocumentSnapshot docRefSnapshots = docRef.GetSnapshotAsync().Result;
                        allData.chick = docRefSnapshots.ConvertTo<ChickData>();
                        listAllData.Add(allData);
                    }
                    else
                    {
                        return StandardStringResult.SaveError();
                    }
                }
                var jsP = JsonConvert.SerializeObject(listAllData);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public string GetById(string docId)
        {
            try
            {
                DocumentReference qry = db.Collection(CName).Document(docId);
                DocumentSnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                FightingData data = new FightingData();
                if (qrySnapshots.Exists)
                {
                    data = qrySnapshots.ConvertTo<FightingData>();
                }
                var jsP = JsonConvert.SerializeObject(data);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string Save(FightingData formData)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document();
                var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";

                return "บันทึกสำเร็จ !!";
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string Update(FightingDataUpdate formData)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(formData.DocId);
                var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";

                return "บันทึกสำเร็จ !!";
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string Delete(string docId)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(docId);
                Dictionary<string, object> updates = new Dictionary<string, object>
                {
                    { "DeleteStatus", 1 }
                };
                var res = docRef.SetAsync(updates, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return "เกิดข้อผิดพลาด กรุณาติดต่อผู้ดูแลระบบ !";

                return "ยกเลิกข้อมูลเรียบร้อย !!";
            }
            catch (Exception e)
            {
                return "Error : " + e;
            }
        }

        public string GetByCockId(string cockId)
        {
            try
            {
                Query qry = db.Collection(CName).WhereEqualTo("DeleteStatus", 0).WhereEqualTo("CockId", cockId).WhereEqualTo("Result", "Win").OrderByDescending("Bet");
                QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                List<FightingData> listQry = new List<FightingData>();
                foreach (DocumentSnapshot dss in qrySnapshots)
                {
                    if (dss.Exists)
                    {
                        var data = dss.ConvertTo<FightingData>();
                        listQry.Add(data);
                    }
                    else
                    {
                        return StandardStringResult.SaveError();
                    }
                }
                var jsP = JsonConvert.SerializeObject(listQry);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }
    }
}
