﻿using GamecockGrading.Middlewares;
using Google.Cloud.Firestore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GamecockGrading.Models
{
    public class VaccineData
    {
        public static FirestoreDb db;
        public static readonly string CName = "Vaccines";

        public VaccineData()
        {
            db = new FirestoreInit().Create();
        }

        [FirestoreData]
        public class VcData
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string VcCode { get; set; }
            [FirestoreProperty]
            public string VcName_Th { get; set; }
            [FirestoreProperty]
            public string VcName_En { get; set; }
            [FirestoreProperty]
            public string VcDetail { get; set; } = "";
            [FirestoreProperty]
            public string HowToUse { get; set; } = "";
            [FirestoreProperty]
            public double LowAgeDay { get; set; } = 0;
            [FirestoreProperty]
            public double LowAgeMonth { get; set; } = 0;
            [FirestoreProperty]
            public double FrequencyMonth { get; set; } = 0;
            [FirestoreProperty]
            public double FrequencyDay { get; set; } = 0;
            [FirestoreProperty]
            public string CreateBy { get; set; }
            [FirestoreProperty]
            public string CreateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
            [FirestoreProperty]
            public string UpdateBy { get; set; }
            [FirestoreProperty]
            public string UpdateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
            [FirestoreProperty]
            public double DeleteStatus { get; set; } = 0;
        }

         [FirestoreData]
        public class VcDataUpdate
        {
            [FirestoreDocumentId]
            public string DocId { get; set; }
            [FirestoreProperty]
            public string VcCode { get; set; }
            [FirestoreProperty]
            public string VcName_Th { get; set; }
            [FirestoreProperty]
            public string VcName_En { get; set; }
            [FirestoreProperty]
            public string VcDetail { get; set; }
            [FirestoreProperty]
            public string HowToUse { get; set; }
            [FirestoreProperty]
            public double LowAgeDay { get; set; } = 0;
            [FirestoreProperty]
            public double LowAgeMonth { get; set; } = 0;
            [FirestoreProperty]
            public double FrequencyMonth { get; set; } = 0;
            [FirestoreProperty]
            public double FrequencyDay { get; set; } = 0;
            [FirestoreProperty]
            public string UpdateBy { get; set; }
            [FirestoreProperty]
            public string UpdateDate { get; set; } = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffff");
        }

        public string List()
        {
            try
            {
                Query qry = db.Collection(CName).WhereEqualTo("DeleteStatus", 0);
                QuerySnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                List<VcData> listQry = new List<VcData>();
                foreach (DocumentSnapshot dss in qrySnapshots)
                {
                    if (dss.Exists)
                    {
                        VcData data = dss.ConvertTo<VcData>();
                        listQry.Add(data);
                    }
                    else
                    {
                        return StandardStringResult.SaveError();
                    }
                }
                var jsP = JsonConvert.SerializeObject(listQry);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public string GetById(string docId)
        {
            try
            {
                DocumentReference qry = db.Collection(CName).Document(docId);
                DocumentSnapshot qrySnapshots = qry.GetSnapshotAsync().Result;
                VcData res = new VcData();
                if (qrySnapshots.Exists)
                {
                    res = qrySnapshots.ConvertTo<VcData>();
                }
                var jsP = JsonConvert.SerializeObject(res);
                Console.WriteLine(jsP);
                return jsP;
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public string Save(VcData formData)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document();
                var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return StandardStringResult.SaveError();

                return StandardStringResult.SaveSuccess();
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public string Update(VcDataUpdate formData)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(formData.DocId);
                var res = docRef.SetAsync(formData, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return StandardStringResult.SaveError();

                return StandardStringResult.SaveSuccess();
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }

        public string Delete(string docId)
        {
            try
            {
                DocumentReference docRef = db.Collection(CName).Document(docId);
                Dictionary<string, object> updates = new Dictionary<string, object>
                {
                    { "DeleteStatus", 1 }
                };
                var res = docRef.SetAsync(updates, SetOptions.MergeAll).Result.UpdateTime.ToString();

                if (res == null)
                    return StandardStringResult.SaveError();

                return "ยกเลิกข้อมูลเรียบร้อย !!";
            }
            catch (Exception e)
            {
                return StandardStringResult.Error(e.Message);
            }
        }
    }
}
