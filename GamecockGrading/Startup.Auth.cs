﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GamecockGrading
{
    public partial class Startup
    {
        private static readonly string secretKey = "!XC!t$kEY!P@$$KEY";
        public static SymmetricSecurityKey signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
        public static string issuer = "connectworkflow.cloud";
        public static string audience = "http://connectworkflow.cloud";

        private void ConfigureServicesJwt(IServiceCollection services)
        {

            var creds = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = issuer,
                ValidAudience = audience,
                IssuerSigningKey = signingKey
            };
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options => {
                        options.Events = new JwtBearerEvents
                        {
                            OnMessageReceived = context =>
                            {
                                context.Token = context.Request.Cookies["access_token"];
                                return Task.CompletedTask;
                            }
                        };
                        options.TokenValidationParameters = tokenValidationParameters;
                    });
        }
        private void ConfigureAuth(IApplicationBuilder app)
        {

            //app.UseSimpleTokenProvider(new TokenProviderOptions
            //{
            //    Path = "/api/account/login",
            //    Issuer = issuer,
            //    Audience = audience,
            //    Expiration = TimeSpan.FromDays(1),
            //    SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
            //    IdentityResolver = new Data().GetIdentity,
            //});
            app.UseAuthentication();
        }
    }

    public static class UserField
    {
        public const string
            UserId = "UserId",
            SiteId = "SiteId",
            CorpId = "CorpId",
            BranchId = "BranchId",
            Email = "Email",
            DisplayName = "DisplayName",
            RoleId = "RoleId",
            AuthType = "AuthType",
            AccessToken = "AccessToken",
            MfaStatus = "MfaStatus",
            PhotoUrl = "PhotoUrl",
            MfaKey = "MfaKey";
    }
}
