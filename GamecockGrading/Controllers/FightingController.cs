﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamecockGrading.Middlewares;
using GamecockGrading.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static GamecockGrading.Models.Fighting;

namespace GamecockGrading.Controllers
{
    [Controller]
    [Route("App/Fighting")]
    public class FightingController : Controller
    {
        [HttpGet("List")]
        public IActionResult List()
        {
            return Json(JsonConvert.DeserializeObject(new Fighting().List()));
        }

        [HttpGet("GetById")]
        public IActionResult GetById(string docId)
        {
            return Json(JsonConvert.DeserializeObject(new Fighting().GetById(docId)));
        }

        [HttpPost("Save")]
        public IActionResult Save(FightingData formData)
        {
            var session = SessionData.GetSession(HttpContext.Session);
            formData.CreateBy = session.UserId;
            formData.UpdateBy = session.UserId;

            if (formData.DocId != null)
            {
                var jsp = JsonConvert.SerializeObject(formData);
                FightingDataUpdate update = JsonConvert.DeserializeObject<FightingDataUpdate>(jsp);
                return Json(new Fighting().Update(update));
            }

            return Json(new Fighting().Save(formData));
        }

        [HttpPost("Delete")]
        public IActionResult Delete(string docId)
        {
            return Json(new Fighting().Delete(docId));
        }

        [HttpGet("GetByCockId")]
        public IActionResult GetByCockId(string cockId)
        {
            return Json(JsonConvert.DeserializeObject(new Fighting().GetByCockId(cockId)));
        }
    }
}