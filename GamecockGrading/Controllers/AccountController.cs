﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using GamecockGrading.Middlewares;
using GamecockGrading.Utilities;
using Google.Rpc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using static GamecockGrading.Middlewares.User;

namespace GamecockGrading.Controllers
{
    [Authorize]
    [Route("Account")]
    public class Account : Controller
    {
        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<JsonResult> Login(Field.CheckAccessToken param)
        {
            try
            {
                var user = await new Data().NewGetIdentity(param);

                if (user.Email == null)
                {
                    return Json(new { Status = false, Message = "User is null", Email = user.UserId });
                }
                else
                {
                    HttpContext.Session.SetString("UserId", user.UserId);
                    HttpContext.Session.SetString("Email", user.Email);
                    HttpContext.Session.SetString("DisplayName", user.DisplayName);
                    HttpContext.Session.SetString("PhotoUrl", user.PhotoUrl);
                    HttpContext.Session.SetString("Role", user.Role);
                    var session = SessionData.GetSession(HttpContext.Session);

                    return Json(new { Status = true });
                }
            }
            catch (Exception e)
            {
                return Json(new { Status = false, Message = e.ToString() });
            }
            //return Json(new { Status = false });
        }

        
    }
}