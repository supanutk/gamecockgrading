﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamecockGrading.Middlewares;
using GamecockGrading.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static GamecockGrading.Models.ChickenData;
using static GamecockGrading.Models.Mating;

namespace GamecockGrading.Controllers
{
    [Controller]
    [Route("App/Mating")]
    public class MatingController : Controller
    {
        [HttpGet("List")]
        public IActionResult List()
        {
            return Json(JsonConvert.DeserializeObject(new Mating().List()));
        }

        [HttpGet("GetById")]
        public IActionResult GetById(string docId)
        {
            return Json(JsonConvert.DeserializeObject(new Mating().GetById(docId)));
        }

        [HttpPost("Save")]
        public IActionResult Save(MatingData formData)
        {
            var session = SessionData.GetSession(HttpContext.Session);
            formData.CreateBy = session.UserId;
            formData.UpdateBy = session.UserId;

            if (formData.DocId != null)
            {
                var jsp = JsonConvert.SerializeObject(formData);
                MatingDataUpdate update = JsonConvert.DeserializeObject<MatingDataUpdate>(jsp);
                return Json(new Mating().Update(update));
            }

            return Json(new Mating().Save(formData));
        }

        [HttpPost("Delete")]
        public IActionResult Delete(string docId)
        {
            return Json(new Mating().Delete(docId));
        }

        [HttpPost("SaveChild")]
        public IActionResult SaveChild(ChickData formData, string docId)
        {
            var session = SessionData.GetSession(HttpContext.Session);
            string status = formData.Status.Substring(0, formData.Status.IndexOf("/"));
            string status_Th = formData.Status.Substring(formData.Status.LastIndexOf("/") + 1);
            formData.CreateBy = session.UserId;
            formData.UpdateBy = session.UserId;
            formData.Status = status;
            formData.Status_Th = status_Th;

            return Json(new Mating().SaveChild(formData, docId));
        }
    }
}