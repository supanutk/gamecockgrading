﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace GamecockGrading.Controllers
{
    [Controller]
    public class Authentication : Controller
    {
        public IActionResult Login(string modalName)
        {

            return View("~/Views/Authentication/Login.cshtml", modalName);
        }
    }
}