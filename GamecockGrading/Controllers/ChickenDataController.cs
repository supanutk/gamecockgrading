﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamecockGrading.Middlewares;
using GamecockGrading.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static GamecockGrading.Models.ChickenData;

namespace GamecockGrading.Controllers
{
    [Controller]
    [Route("App/ChickenData")]
    public class ChickenDataController : Controller
    {
        [HttpGet("List")]
        public IActionResult List()
        {
            return Json(JsonConvert.DeserializeObject(new ChickenData().List()));
        }

        [HttpGet("ListAlive")]
        public IActionResult ListAlive()
        {
            return Json(JsonConvert.DeserializeObject(new ChickenData().ListAlive()));
        }

        [HttpGet("GetById")]
        public IActionResult GetById(string docId)
        {
            return Json(JsonConvert.DeserializeObject(new ChickenData().GetById(docId)));
        }

        [HttpGet("GetBySex")]
        public IActionResult GetBySex(string sex)
        {
            return Json(JsonConvert.DeserializeObject(new ChickenData().GetBySex(sex)));
        }

        [HttpPost("Save")]
        public IActionResult Save(ChickData formData)
        {
            var session = SessionData.GetSession(HttpContext.Session);
            formData.CreateBy = session.UserId;
            formData.UpdateBy = session.UserId;

            if (formData.DocId != null)
            {
                var jsp = JsonConvert.SerializeObject(formData);
                ChickDataUpdate update = JsonConvert.DeserializeObject<ChickDataUpdate>(jsp);
                return Json(new ChickenData().Update(update));
            }

            return Json(new ChickenData().Save(formData));
        }

        [HttpPost("UpdateStatusToDead")]
        public IActionResult UpdateStatusToDead(string docId)
        {
            return Json(new ChickenData().UpdateStatusToDead(docId));
        }

        [HttpPost("UpdateStatusToSold")]
        public IActionResult UpdateStatusToSold(ChickDataToSold update)
        {
            return Json(new ChickenData().UpdateStatusToSold(update));
        }

        [HttpPost("Delete")]
        public IActionResult Delete(string docId)
        {
            return Json(new ChickenData().Delete(docId));
        }

        [HttpGet("GetFullDetail")]
        public IActionResult GetFullDetail(string docId)
        {
            return Json(JsonConvert.DeserializeObject(new ChickenData().GetFullDetail(docId)));
        }

        [HttpGet("GetCockFullDetail")]
        public IActionResult GetCockFullDetail(string docId)
        {
            return Json(JsonConvert.DeserializeObject(new ChickenData().GetCockFullDetail()));
        }

        [HttpGet("GetUrlPic")]
        public IActionResult GetUrlPic(string fileName)
        {
            return Json(FirestoreInit.GenSinger(fileName));
        }

        [HttpGet("CheckSameCode/{code}")]
        public IActionResult CheckSameCode(string code)
        {
            return Json(new ChickenData().CheckSameCode(code));
        }

        #region Remove Chickens
        [HttpGet("RemoveChickens")]
        public IActionResult RemoveChickens()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/ChickenData/" + session.Role + "/RemoveChickens.cshtml");
        }
        #endregion
    }
}