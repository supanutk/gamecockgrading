﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamecockGrading.Middlewares;
using GamecockGrading.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GamecockGrading.Controllers
{
    [Controller]
    [Route("App/Reports")]
    public class ReportsController : Controller
    {
        #region ByFighting
        [HttpGet("ByFighting")]
        public IActionResult ByFighting()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Reports/" + session.Role + "/ByFighting.cshtml");
        }
        #endregion

        #region ByMating
        [HttpGet("ByMating")]
        public IActionResult ByMating()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Reports/" + session.Role + "/ByMating.cshtml");
        }

        [HttpGet("ByMating/ListChickForReport")]
        public IActionResult ListChickForReport()
        {
            return Json(JsonConvert.DeserializeObject(new ChickenData().ListChickForReport()));
        }

        [HttpGet("ByMating/GetListChickById")]
        public IActionResult GetListChickById(string docId, string sex)
        {
            return Json(JsonConvert.DeserializeObject(new ChickenData().GetListChickById(docId, sex)));
        }

        #endregion

        #region ByReward
        [HttpGet("ByReward")]
        public IActionResult ByReward()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Reports/" + session.Role + "/ByReward.cshtml");
        }
        #endregion

        #region ByReward
        [HttpGet("CheckByVaccine")]
        public IActionResult CheckByVaccine()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Reports/" + session.Role + "/CheckByVaccine.cshtml");
        }
        #endregion

        #region PrintByFighting
        [HttpGet("PrintByFighting")]
        public IActionResult PrintByFighting()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Reports/" + session.Role + "/PrintByFighting.cshtml");
        }
        #endregion

        #region PrintByMating
        [HttpGet("PrintByMating")]
        public IActionResult PrintByMating()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Reports/" + session.Role + "/PrintByMating.cshtml");
        }
        #endregion

        #region PrintByReward
        [HttpGet("PrintByReward")]
        public IActionResult PrintByReward()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Reports/" + session.Role + "/PrintByReward.cshtml");
        }
        #endregion

        #region PrintAllChickData
        [HttpGet("PrintAllChickData")]
        public IActionResult PrintAllChickData()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Reports/" + session.Role + "/PrintAllChickData.cshtml");
        }
        #endregion
    }
}