﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamecockGrading.Middlewares;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static GamecockGrading.Middlewares.LastValue;

namespace GamecockGrading.Controllers
{
    [Controller]
    [Route("App/Api")]
    public class ApiController : Controller
    {
        #region Last Value

        [Route("LogLastValue/List")]
        [HttpGet]
        public IActionResult ListLogLastValue()
        {
            var session = SessionData.GetSession(HttpContext.Session);
            return Json(JsonConvert.DeserializeObject(new LastValue().List(session)));
        }

        [Route("LogLastValue/Save")]
        [HttpPost]
        public void SaveLogLastValue(string jsLastValue)
        {
            var data = JsonConvert.DeserializeObject<LastValUpdate>(jsLastValue);
            var session = SessionData.GetSession(HttpContext.Session);
            new LastValue().Save(session, data);
        }

        #endregion
    }
}
