﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GamecockGrading.Models;
using GamecockGrading.Middlewares;

namespace GamecockGrading.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var session = SessionData.GetSession(HttpContext.Session);
            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            ViewBag.DisplayName = session.UserId;
            return View("~/Views/App/Index.cshtml");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
