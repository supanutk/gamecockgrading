﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamecockGrading.Middlewares;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static GamecockGrading.Middlewares.User;

namespace GamecockGrading.Controllers
{
    [Controller]
    [Route("App/Security")]
    public class SecurityController : Controller
    {

        [HttpGet("ListAllUser")]
        public IActionResult ListAllUser()
        {
            return Json(JsonConvert.DeserializeObject(new User().ListAllUser()));
        }

        [HttpPost("ChangeRole")]
        public IActionResult ChangeRole(UserData formData)
        {
            var session = SessionData.GetSession(HttpContext.Session);
            if (session.Role != "Admin")
                return RedirectToAction("Index", "App");

            if (formData.DocId != null)
            {
                var jsp = JsonConvert.SerializeObject(formData);
                var update = JsonConvert.DeserializeObject<UserDataUpdate>(jsp);
                return Json(new User().ChangeRole(update));
            }

            return Json(new User().SaveFromView(formData));
        }

        [HttpGet("GetUserById")]
        public IActionResult GetUserById(string docId)
        {
            return Json(JsonConvert.DeserializeObject(new User().GetUserById(docId)));
        }

        [HttpGet("ApproveUser")]
        public IActionResult Security()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");
            if (session.Role != "Admin")
                return RedirectToAction("Index", "App");

            return View("~/Views/App/Security/" + session.Role + "/ApproveUser.cshtml");
        }

        [HttpGet("ApproveUser/ListPendingUser")]
        public IActionResult ListPendingUser()
        {
            var session = SessionData.GetSession(HttpContext.Session);
            if (session.Role != "Admin")
                return RedirectToAction("Index", "App");

            return Json(JsonConvert.DeserializeObject(new User().ListPendingUser()));
        }

        [HttpPost("ApproveUser/ChangeStatus")]
        public IActionResult ChangeStatus(string docId, string status)
        {
            var session = SessionData.GetSession(HttpContext.Session);
            if (session.Role != "Admin")
                return RedirectToAction("Index", "App");

            return Json(new User().ChangeStatus(docId, status));
        }

        [HttpPost("AncientLogin")]
        public IActionResult AncientLogin(string username,string password)
        {
            var res = new User().AncientLogin(username, password);
            if (res == null)
                return Json(null);

            UserData user = JsonConvert.DeserializeObject<UserData>(res);
            HttpContext.Session.SetString("UserId", user.Email);
            HttpContext.Session.SetString("Email", user.Email);
            HttpContext.Session.SetString("DisplayName", user.Name);
            HttpContext.Session.SetString("PhotoUrl", "https://img.icons8.com/ios-glyphs/60/000000/user--v1.png");
            HttpContext.Session.SetString("Role", user.Role);
            return Json(JsonConvert.DeserializeObject(res));
        }

        [HttpPost("Register")]
        public IActionResult Register(UserData formData)
        {
            formData.Status = "Pending";
            formData.Role = "User";

            return Json(new User().SaveRegister(formData));
        }

        [HttpGet("CheckUsername")]
        public IActionResult CheckUserName(string username)
        {
            return Json(new User().CheckUsername(username));
        }

        [HttpGet("CheckEmail")]
        public IActionResult CheckEmail(string email)
        {
            return Json(new User().CheckUser(email));
        }
    }
}