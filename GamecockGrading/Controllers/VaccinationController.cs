﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamecockGrading.Middlewares;
using GamecockGrading.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static GamecockGrading.Models.Vaccination;
using static GamecockGrading.Models.VaccineData;

namespace GamecockGrading.Controllers
{
    [Controller]
    [Route("App/Vaccination")]
    public class VaccinationController : Controller
    {
        #region VaccineData
        [HttpGet("VaccineData")]
        public IActionResult Vaccination()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Vaccination/" + session.Role + "/VaccineData.cshtml");
        }

        [HttpGet("VaccineData/List")]
        public IActionResult List()
        {
            return Json(JsonConvert.DeserializeObject(new VaccineData().List()));
        }

        [HttpGet("VaccineData/GetById")]
        public IActionResult GetById(string docId)
        {
            return Json(JsonConvert.DeserializeObject(new VaccineData().GetById(docId)));
        }

        [HttpPost("VaccineData/Save")]
        public IActionResult Save(VcData formData)
        {
            var session = SessionData.GetSession(HttpContext.Session);
            formData.CreateBy = session.UserId;
            formData.UpdateBy = session.UserId;

            if (formData.DocId != null)
            {
                var jsp = JsonConvert.SerializeObject(formData);
                VcDataUpdate update = JsonConvert.DeserializeObject<VcDataUpdate>(jsp);
                return Json(new VaccineData().Update(update));
            }

            return Json(new VaccineData().Save(formData));
        }

        [HttpPost("VaccineData/Delete")]
        public IActionResult Delete(string docId)
        {
            return Json(new VaccineData().Delete(docId));
        }
        #endregion

        #region Vaccination
        [HttpGet("List")]
        public IActionResult ListVaccination()
        {
            return Json(JsonConvert.DeserializeObject(new Vaccination().List()));
        }

        [HttpGet("GetById")]
        public IActionResult GetVaccinationById(string docId)
        {
            return Json(JsonConvert.DeserializeObject(new Vaccination().GetById(docId)));
        }

        [HttpPost("Save")]
        public IActionResult SaveVaccination(VaccinationData formData, string[] chickens)
        {
            var session = SessionData.GetSession(HttpContext.Session);
            formData.CreateBy = session.UserId;
            formData.UpdateBy = session.UserId;
            formData.Chickens = chickens;

            if (formData.DocId != null)
            {
                var jsp = JsonConvert.SerializeObject(formData);
                VaccinationUpdate update = JsonConvert.DeserializeObject<VaccinationUpdate>(jsp);
                return Json(new Vaccination().Update(update));
            }

            return Json(new Vaccination().Save(formData));
        }

        [HttpPost("Delete")]
        public IActionResult DeleteVaccination(string docId)
        {
            return Json(new Vaccination().Delete(docId));
        }

        [HttpGet("ListChickLastVac")]
        public IActionResult ListChickLastVac(string vcDocId)
        {
            return Json(JsonConvert.DeserializeObject(new Vaccination().ListChickLastVac(vcDocId)));
        }
        
        [HttpGet("ListAllChickAllLastVac")]
        public IActionResult ListAllChickAllLastVac()
        {
            return Json(JsonConvert.DeserializeObject(new Vaccination().ListAllChickAllLastVac()));
        }
        #endregion
    }
}