﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GamecockGrading.Middlewares;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GamecockGrading.Controllers
{
    [Controller]
    [Route("App")]
    public class AppController : Controller
    {
        [Route("Index")]
        public IActionResult Index()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Index.cshtml");
        }

        [HttpGet("Logout/{modalName?}")]
        public IActionResult Logout(string modalName)
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login", "Authentication", new { modalName = modalName });
        }

        [HttpGet("ChickenData")]
        public IActionResult ChickenData()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/ChickenData/" + session.Role + "/ChickenData.cshtml");
        }
        
        [HttpGet("Vaccination")]
        public IActionResult Vaccination()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Vaccination/" + session.Role + "/Vaccination.cshtml");
        }
        
        [HttpGet("Mating")]
        public IActionResult Mating()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Mating/" + session.Role + "/Mating.cshtml");
        }
        
        [HttpGet("Fighting")]
        public IActionResult Fighting()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Fighting/" + session.Role + "/Fighting.cshtml");
        }

        [HttpGet("Reports")]
        public IActionResult Reports()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Reports/" + session.Role + "/AllReports.cshtml");
        }

        [HttpGet("Security")]
        public IActionResult Security()
        {
            var session = SessionData.GetSession(HttpContext.Session);

            if (session.UserId == null)
                return RedirectToAction("Login", "Authentication");

            return View("~/Views/App/Security/" + session.Role + "/Security.cshtml");
        }
    }
}