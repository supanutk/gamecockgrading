﻿function ekUpload() {
    function Init() {
        console.log("Upload Initialised");

        var fileSelect = $('[name="fileUpload"]')
        fileSelect.on('change', fileSelectHandler)
    }

    function fileSelectHandler(e) {
        // Fetch FileList object
        var col = $(e.currentTarget).closest('[class*="col-"]')
        var files = e.target.files || e.dataTransfer.files;
        parseFile(col, e.target.files[0]);
    }


    function parseFile(col, file) {

        console.log(file);
        fr = new FileReader();
        fr.onload = function () {
            var data = fr.result.split(",").pop()
            $('#modal_Add').find('.overlay').show();
            console.log(data);
            $.ajax({
                url: 'https://us-central1-gamecockgrading.cloudfunctions.net/fileUpload/uploadimg',
                method: 'POST',
                data: {
                    fileName: file.name,
                    file: data
                },
                success: function (res) {
                    console.log(res);

                    var imageName = file.name;

                    var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
                    console.log(isGood);
                    if (isGood) {
                        col.find('.start').addClass("hidden");
                        col.find('.response').removeClass("hidden");
                        col.find('[name="notimage"]').addClass("hidden");
                        // Thumbnail Preview
                        col.find('[name="file-image"]').removeClass("hidden");
                        col.find('[name="file-image"]').attr('src', URL.createObjectURL(file));
                        col.find('.fileName').val(file.name)
                    }
                    $('#modal_Add').find('.overlay').hide();

                },
                error: function (err) { console.log('Error : ' + err) },
            });
        }
        fr.readAsDataURL(file);

    }
    // Check for the various File API support.
    if (window.File && window.FileList && window.FileReader) {
        Init();
    }
}

$(function () {
    ekUpload();
})