﻿function calBetweenTwoDate(startDate, endDate) {
    var st = new Date(startDate)
    var ed = new Date(endDate)
    var res = new Date(ed - st)
    var diff = res / (1000 * 60 * 60 * 24)
    return diff
}