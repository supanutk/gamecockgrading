﻿var inactivityTime = function () {
    var time;
    window.onload = resetTimer;
    // DOM Events
    document.onload = resetTimer;
    document.onmousemove = resetTimer;
    document.onmousedown = resetTimer; // touchscreen presses
    document.ontouchstart = resetTimer;
    document.onclick = resetTimer;     // touchpad clicks
    document.onkeypress = resetTimer;
    document.onkeydown = resetTimer;
    document.onkeyup = resetTimer;
    document.onchange = resetTimer;
    document.onclose = resetTimer;
    document.oncopy = resetTimer;
    document.oncut = resetTimer;
    document.ondblclick = resetTimer;
    document.ondrop = resetTimer;
    document.onfocus = resetTimer;
    document.addEventListener('onload', resetTimer, true); // improved; see comments

    function logout() {
        //alert('คุณขาดการเชื่อมต่อเกิน 1 ชั่วโมง\nกรุณา Login ใหม่อีกครั้ง !!')
        location.href = '/App/Logout/modalIdleSession'
        $('#modalIdleSession').modal('show')
    }

    function resetTimer() {
        clearTimeout(time);
        time = setTimeout(logout, 60 * 60 * 1000) // แก้แค่ตัวเลขชุดแรก 
    }
};

window.onload = function () {
    inactivityTime();
}