﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace GamecockGrading.Utilities
{
    public static class IdentityConverter
    {
        public static T To<T>(this IIdentity Identity)
        {
            return ((ClaimsIdentity)Identity).To<T>();
        }

        public static T To<T>(this ClaimsIdentity obj)
        {
            try
            {
                if (obj != null)
                {
                    var model = (T)Activator.CreateInstance(typeof(T));

                    foreach (PropertyInfo pi in model.GetType().GetProperties())
                    {

                        pi.SetValue(model, Convert.ChangeType(obj.FindFirst(pi.Name).Value, pi.PropertyType));
                    }
                    return model;
                }
            }
            catch
            {
            }
            return default;
        }

        public static List<Claim> FromObject(object obj)
        {
            List<Claim> claims = new List<Claim>();
            try
            {
                if (obj != null)
                {
                    foreach (PropertyInfo pi in obj.GetType().GetProperties())
                    {

                        claims.Add(new Claim(pi.Name, pi.GetValue(obj) == null ? string.Empty : pi.GetValue(obj).ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                claims = null;
                throw ex;
            }
            return claims;
        }
    }
}
